package com.example.machine.controller;

import com.example.machine.models.User;
import com.example.machine.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class User_Controller {

    @Autowired
    UserService userService;


    @GetMapping("users")
    public ResponseEntity<List<User>> getUsers() {
      return ResponseEntity.ok(userService.getUser());
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody User user) {
        try {
            userService.createUser(user);
            return ResponseEntity.ok("Votre utilisateur est crée avec succès");
        } catch (IllegalArgumentException | IllegalAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body("Information saisi sont erronés"+e.getMessage());
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Erreur :"+e.getMessage());
        }
    }

    @PostMapping("connection")
    public ResponseEntity<?> connected(@RequestParam String login,
                                       @RequestParam String password) {
        try {
             userService.connection(login, password);
            return ResponseEntity.ok("BIENVENU SUR NOTRE SITE VOUS ETES BIEN CONNECTE");
        } catch (IllegalArgumentException | IllegalAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body("Information: "+e.getMessage());
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Erreur :"+e.getMessage());
        }
    }

    @GetMapping("calcule")
    public void calcule() {
        try {
            userService.petitCalcule();
            System.out.println("OK");
        } catch (RuntimeException e) {
            System.out.println("ERROR");  // "ERROR" is printed as expected
        }
    }
}
