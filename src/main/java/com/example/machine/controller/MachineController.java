package com.example.machine.controller;

import com.example.machine.models.Beer;
import com.example.machine.models.Cafe;
import com.example.machine.models.The;
import com.example.machine.services.BeerService;
import com.example.machine.services.CafeService;
import com.example.machine.services.TheService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "API DE TEST CACIB")
@RequestMapping("api")
public class MachineController {

    @Autowired
    CafeService cafeService;

    @Autowired
    TheService theService;

    @Autowired
    BeerService beerService;

    @GetMapping("")
    public ResponseEntity<List<Object>> getMachine() {
        List<Object> objects = List.of(new Cafe(), new The(), new Beer());
       return ResponseEntity.ok(objects);
    }

    @GetMapping("{text}")
    @ApiOperation(value = "pour choisir sa boisson")
    public ResponseEntity<String> getCafe(@PathVariable(value = "text", required = true) String text) {
        try {
                if (text.equals("cafe")) {
                    return ResponseEntity.ok(cafeService.getCafe(text));
                } else if (text.equals("the")) {
                    return ResponseEntity.ok(theService.getThe(text));
                }
                if (text.equals("biere")) {
                    return ResponseEntity.ok(beerService.getBiere(text));
                } else {
                    throw new IllegalArgumentException();
                }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Erreur globale");
        }
    }

}
