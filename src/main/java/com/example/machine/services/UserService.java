package com.example.machine.services;

import com.example.machine.models.Beer;
import com.example.machine.models.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    Singleton instance = Singleton.getInstance();

    /**
     * This methode is to get userList
     *
     * @return
     */
    public List<User> getUser() {
        return instance.getUser();
    }

    /**
     * this methode is to create User
     *
     * @param user
     * @throws IllegalAccessException
     */
    public void createUser(User user) throws IllegalAccessException {
        instance.addUser(user);
    }

    /**
     * If you want to connect here the login is Cacib and password is cacib
     *
     * @param login
     * @param password
     * @return
     * @throws IllegalAccessException
     */
    public String connection(String login, String password) throws IllegalAccessException {
        boolean connection = instance.connection(login, password);
        try {
            if(connection == true){
                return "FELICITATION VOUS VENEZ DE VOUS CONNECTER";
            }else {
                throw new IllegalAccessException("Les identifiants saisis sont incorrectes");
            }
        }finally {
            System.out.println("Ca a fini d'excuter");
        }

    }

    /**
     * this method most check user
     */
    public void checkUser() {
        User user = null;
        if(user.getLogin() == "cacib") {
            System.out.println("C'est beau la vie");
        }
    }


    public  void petitCalcule() {
        try {
            throw new RuntimeException();
        } finally {
            for (int i = 0; i < 10; i ++) {
               int q= 9;
                if (q == i) {
                    System.out.println("Ca a fini d'excuter");
                    break;
                }
            }
        }
    }
}
