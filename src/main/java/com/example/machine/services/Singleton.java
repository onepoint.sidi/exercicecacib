package com.example.machine.services;

import com.example.machine.models.User;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

public class Singleton {
    private static final Singleton SINGLETON = new Singleton();
    private final List<User> users = new ArrayList<>();
    @Value("password.value")
    private String passwordValue;

    private Singleton() {
    }

    public static Singleton getInstance() {
        return SINGLETON;
    }

    public void addUser(User user) {
        if (users.contains(user)) {
            throw new IllegalStateException("Utilisateur existe deja");
        }
        synchronized (users) {
            users.add(user);
        }
    }

    public List<User> getUser() {
        return users;
    }

    public boolean connection(String login, String password) throws IllegalAccessException {
        if (login != null) {
            if (login == "cacib") {
                if (password != null) {
                    if (password == passwordValue) {
                        return true;
                    } else {
                        return false;
                    }
                }else{
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
