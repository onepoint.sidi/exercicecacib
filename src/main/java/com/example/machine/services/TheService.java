package com.example.machine.services;

import com.example.machine.models.Cafe;
import com.example.machine.models.The;
import org.springframework.stereotype.Service;

@Service
public class TheService {

    public String getThe(String the) {

        if(the.equals(null)){
            throw new IllegalArgumentException("votre choix n'est pas valide!");
        }
        if(the.equals("the")){
            The the1 = new The();
            return "Votre "
                    .concat(the1.getType()
                            .concat(" est pres avec ")
                            .concat(String.valueOf(the1.getNbrSucre()))+" sucre");
        }else {
            return "votre choix n'est pas valide!";
        }

    }
}
