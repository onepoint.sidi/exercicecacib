package com.example.machine.services;

import com.example.machine.models.Cafe;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class CafeService {

    public String getCafe(String cafe) {

        if(cafe.equals(null)){
            throw new IllegalArgumentException("votre choix n'est pas valide!");
        }
        Cafe cafe1 = new Cafe();
        return "Votre café "
                .concat(cafe1.getType()
                        .concat(" est pres avec du sucre"));
    }

    /**
     *
     *  @Override
     *     public String format(LoggingEvent event) {
     *         JsonLog jsonLog = new JsonLog();
     *         jsonLog.setApplication(this.application);
     *         jsonLog.setComponent(this.component);
     *         jsonLog.setHost(CmtLogUtil.HOST_NAME);
     *         jsonLog.setHostIp(CmtLogUtil.HOST_IP);
     *         jsonLog.setTimestamp(event.timeStamp);
     *         jsonLog.setLevel(event.getLevel().toString());
     *         jsonLog.setMessage(super.format(event));
     *         if (event.getThrowableInformation() != null) {
     *             jsonLog.setException(CmtLogUtil.extractException(event.getThrowableInformation().getThrowable()));
     *         }
     *
     *         jsonLog.setThreadName(event.getThreadName());
     *         jsonLog.setProperties(this.properties);
     *         if (this.contextData) {
     *             jsonLog.setContextData(event.getProperties());
     *         }
     *
     *         if (this.locationInfo) {
     *             jsonLog.setLocation(this.extractLocation(event.getLocationInformation()));
     *         }
     *
     *         if (event.getLevel().equals(Level.INFO)) {
     *             jsonLog.setCustomData(CmtLogUtil.extractCustomFields(customFields,super.format(event)));
     *         }
     *         try {
     *             return CmtLogUtil.OBJECT_MAPPER.writeValueAsString(jsonLog);
     *         } catch (JsonProcessingException var4) {
     *             throw new IllegalStateException("Unable to format logging event", var4);
     *         }
     *     }
     */


}
