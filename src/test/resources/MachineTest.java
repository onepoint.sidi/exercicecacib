package resources;

import com.example.machine.models.Beer;
import com.example.machine.models.Cafe;
import com.example.machine.models.The;
import com.example.machine.services.BeerService;
import com.example.machine.services.CafeService;
import com.example.machine.services.TheService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.beans.beancontext.BeanContext;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MachineTest {
    CafeService cafeService;
    TheService theService;
    BeerService beerService;
    public MachineTest(){
        cafeService  = new CafeService();
        theService = new TheService();
        beerService = new BeerService();
    }
    Cafe cafe = new Cafe();
    The the = new The();
    Beer beer = new Beer();
    List<Object> listOption = List.of(cafe, the, beer);
    //Injection du service café

    @Test
    public void getMachineChoose_shouldReturnAListOfChoose(){

        // Café
        Cafe caf = new Cafe("Expresso", "oui");

        // the
        The t = new The("Infusion",2);
        //Beer
        Beer b = new Beer("oui", "12%");

        List<Object> list = new ArrayList<>();
        list.add(caf);
        list.add(t);
        list.add(b);

        Assert.assertEquals(list, listOption);
    }

    @Test
    public void getCafe_shouldReturnCafe(){
        // Café
        Cafe caf = new Cafe();
        String cafe1 = cafeService.getCafe("cafe");

        Assert.assertEquals(cafe1, "Votre café "
                .concat(caf.getType())
                        .concat(" est pres avec du sucre"));
    }

    @Test
    public void getThe_shouldReturnThe(){
        // Café
        The the = new The();
        String the1 = theService.getThe("the");

        Assert.assertEquals(the1, "Votre "
                .concat(the.getType()
                        .concat(" est pres avec "+the.getNbrSucre()+" sucre")));
    }

    @Test
    public void getBeer_shouldReturnBeer(){
        // Café
        Beer beer = new Beer();
        String beer1 = beerService.getBiere("biere");

        Assert.assertEquals(beer1,"Votre Bière est pres avec de l'alcool de "
                .concat(beer.getAlcohol()));
    }



}
